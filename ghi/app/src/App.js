import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleModelsForm from './inventory/VehicleModelsForm';
import VehicleModels from './inventory/VehicleModels';
import Automobiles from './inventory/AutomobilesList';
import AutomobilesForm from './inventory/AutomobilesForm';
import TechnicianForm from './services/TechnicianForm';
import AppointmentsForm from './services/AppointmentsForm';
import AppointmentsList from './services/AppointmentsList'
import SalesPersonForm from './sales/SalesPersonForm';
import PotentialCustomerForm from './sales/PotentialCustomerForm';
import React from 'react';
import ManufacturersForm from './inventory/ManufacturersForm';
import Manufacturers from './inventory/Manufacturers';
import SearchHistory from './services/ServiceHistory';
import SalesRecordForm from './sales/SalesRecordForm';
import SalesRecordList from './sales/SalesRecordList';
import SalesHistory from './sales/SalesHistory';
import Footer from './Footer';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      models: [],
      autos: [],
      appointments: [],
      manufacturers: [],
      sales: [],
    };

    this.loadVehicleModels = this.loadVehicleModels.bind(this);
    this.loadAutomobiles = this.loadAutomobiles.bind(this);
    this.loadAppointments = this.loadAppointments.bind(this);
    this.loadManufacturers = this.loadManufacturers.bind(this);
    this.loadSales = this.loadSales.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onFinish = this.onFinish.bind(this);
    this.ManufacturerOnDelete = this.ManufacturerOnDelete.bind(this);
    this.VehicleModelOnDelete = this.VehicleModelOnDelete.bind(this);
    this.AutoOnDelete = this.AutoOnDelete.bind(this);
    this.SaleOnDelete = this.SaleOnDelete.bind(this);
  }

  async loadVehicleModels() {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  async loadSales() {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ sales: data.sales });
    }
  }

  async loadAutomobiles() {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ autos: data.autos });
    }
  }

  async loadAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ appointments: data.appointments });
    }
  }

  async onCancel(appointment) {
    if (window.confirm('Are you sure you finished this appointment?')) {
      const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/cancelled`
      const fetchConfig = {
        method: "put",
      }
      const response = await fetch(appointmentUrl, fetchConfig)
      if (response.ok) {
        const newAppointments = this.state.appointments.filter((appoint) => appoint.id !== appointment.id)
        this.setState({ appointments: newAppointments })
      }
    }
  }

  async loadManufacturers() {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers })
    }
  }

  async onFinish(appointment) {
    if (window.confirm('Are you sure you finished this appointment?')) {
      const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/finished`
      const fetchConfig = {
        method: "put",
      }
      const response = await fetch(appointmentUrl, fetchConfig)
      if (response.ok) {
        const newAppointments = this.state.appointments.filter((appoint) => appoint.id !== appointment.id)
        this.setState({ appointments: newAppointments })
      }
    }
  }

  async ManufacturerOnDelete(manufacturer) {
    if (window.confirm('Are you sure you want to delete this manufacturer?')) {
      const manufacturerUrl = `http://localhost:8100/api/manufacturers/${manufacturer.id}/`
      const fetchConfig = {
        method: "delete",
      }
      const response = await fetch(manufacturerUrl, fetchConfig)
      if (response.ok) {
        const newManufacturers = this.state.manufacturers.filter((manufacture) => manufacture.id !== manufacturer.id)
        this.setState({ manufacturers: newManufacturers })
      }
    }
  }

  async VehicleModelOnDelete(model) {
    if (window.confirm('Are you sure you want to delete this model?')) {
      const modelUrl = `http://localhost:8100/api/models/${model.id}/`
      const fetchConfig = {
        method: "delete",
      }
      const response = await fetch(modelUrl, fetchConfig)
      if (response.ok) {
        const newModels = this.state.models.filter((mod) => mod.id !== model.id)
        this.setState({ models: newModels })
      }
    }
  }

  async AutoOnDelete(auto) {
    if (window.confirm('Are you sure you want to delete this automobile?')) {
      const autoUrl = `http://localhost:8100/api/automobiles/${auto.vin}/`
      const fetchConfig = {
        method: "delete",
      }
      const response = await fetch(autoUrl, fetchConfig)
      if (response.ok) {
        const newAutos = this.state.autos.filter((automobile) => automobile.vin !== auto.vin)
        this.setState({ autos: newAutos })
      }
    }
  }

  async SaleOnDelete(sale) {
    if (window.confirm('Are you sure you want to delete this sale record?')) {
      const saleUrl = `http://localhost:8090/api/sales/${sale.id}/`
      const fetchConfig = {
        method: "delete",
      }
      const response = await fetch(saleUrl, fetchConfig)
      if (response.ok) {
        const newSales = this.state.sales.filter((saleRecord) => saleRecord.id !== sale.id)
        this.setState({ sales: newSales })
      }
    }
  }

  async componentDidMount() {
    this.loadVehicleModels()
    this.loadAutomobiles()
    this.loadAppointments()
    this.loadManufacturers()
    this.loadSales()
  }

  render() {
    return (
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="models/">
              <Route path="" element={<VehicleModels models={this.state.models} VehicleModelOnDelete={this.VehicleModelOnDelete} />} />
              <Route path="new" element={<VehicleModelsForm load={this.loadVehicleModels} />} />
            </Route>
            <Route path="manufacturers/">
              <Route path="" element={<Manufacturers manufacturers={this.state.manufacturers} ManufacturerOnDelete={this.ManufacturerOnDelete} />} />
              <Route path="new" element={<ManufacturersForm load={this.loadManufacturers} />} />
            </Route>
            <Route path="automobiles/">
              <Route path="" element={<Automobiles autos={this.state.autos} AutoOnDelete={this.AutoOnDelete} />} />
              <Route path="new" element={<AutomobilesForm load={this.loadAutomobiles} />} />
            </Route>
            <Route path="technicians/new" element={<TechnicianForm />} />
            <Route path="salespersons/new" element={<SalesPersonForm />} />
            <Route path="sales/" element={<SalesRecordList sales={this.state.sales} SaleOnDelete={this.SaleOnDelete} />} />
            <Route path="sales/new" element={<SalesRecordForm load={this.loadSales} />} />
            <Route path="potential_customers/new" element={<PotentialCustomerForm />} />
            <Route path="salespersons/history" element={<SalesHistory />} />
            <Route path="appointments/">
              <Route path="" element={<AppointmentsList appointments={this.state.appointments} onCancel={this.onCancel} onFinish={this.onFinish} />} />
              <Route path="new" element={<AppointmentsForm load={this.loadAppointments} />} />
              <Route path="history" element={<SearchHistory appointments={this.state.appointments} onSearch={this.onSearch} />} />
            </Route>
          </Routes>
        </div>
        <Footer />
      </BrowserRouter>
    )
  }
}

export default App;
