import { NavLink } from 'react-router-dom';
import { menuItems } from './dropdown/menuItems';
import MenuItems from './dropdown/MenuItem';
import logo from './images/logo.png';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-light border-bottom">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          <img src={logo} style={{ height: "70px" }} alt="logo" />
        </NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse text-primary" id="navbarSupportedContent">
          <ul className="menus">
            {menuItems.map((menu, index) => {
              return <MenuItems items={menu} key={index} />
            })}
          </ul>

        </div>
      </div>
    </nav>
  )
}

export default Nav;
