import React from 'react'

class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobiles: [],
            salesPersons: [],
            potentialCustomers: [],
            salesPrice: '',
        }

        this.handleAutomobilesChange = this.handleAutomobilesChange.bind(this)
        this.handleSalesPersonsChange = this.handleSalesPersonsChange.bind(this)
        this.handleCustomersChange = this.handleCustomersChange.bind(this)
        this.handleSalesPriceChange = this.handleSalesPriceChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.potential_customer = data.potentialCustomer;
        data.sales_price = data.salesPrice;
        data.sales_person = data.salesPerson;
        delete data.potentialCustomer;
        delete data.salesPrice;
        delete data.salesPerson;
        delete data.automobiles;
        delete data.salesPersons;
        delete data.potentialCustomers;
        const salesRecordUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(salesRecordUrl, fetchConfig)
        if (response.ok) {
            alert('New sales record created!')
            const newSalesRecord = await response.json()
            const cleared = {
                automobile: '',
                salesPerson: '',
                potentialCustomer: '',
                salesPrice: '',
            };
            this.setState(cleared);
            this.props.load();
        }
    }

    handleAutomobilesChange(event) {
        const value = event.target.value
        this.setState({ automobile: value })
    }

    handleSalesPersonsChange(event) {
        const value = event.target.value
        this.setState({ salesPerson: value })
    }

    handleCustomersChange(event) {
        const value = event.target.value
        this.setState({ potentialCustomer: value })
    }

    handleSalesPriceChange(event) {
        const value = event.target.value
        this.setState({ salesPrice: value })
    }

    async loadCustomers() {
        const response = await fetch("http://localhost:8090/api/potential_customers/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ potentialCustomers: data.potential_customers });
        }
    }

    async loadAutos() {
        const response = await fetch("http://localhost:8090/api/automobile_vos/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ automobiles: data });
        }
    }

    async loadSalesPersons() {
        const response = await fetch("http://localhost:8090/api/sales_persons/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ salesPersons: data.sales_persons });
        }
    }


    async componentDidMount() {
        this.loadCustomers();
        this.loadAutos();
        this.loadSalesPersons();
    }


    render() {
        return (
            <div className="my-5 container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a sales record</h1>
                            <form onSubmit={this.handleSubmit} id="create-sales-record-form">
                                <div className="mb-3">
                                    <select onChange={this.handleAutomobilesChange} value={this.state.automobile} name="automobile" id="automobile" className='form-select' required>
                                        <option value="">Choose an automobile</option>
                                        {this.state.automobiles.map(auto => {
                                            return (
                                                <option key={auto.id} value={auto.id}>
                                                    {auto.vin}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleSalesPersonsChange} value={this.state.salesPerson} name="sales_person" id="sales_person" className='form-select' required>
                                        <option value="">Choose a sales person</option>
                                        {this.state.salesPersons.map(salesPerson => {
                                            return (
                                                <option key={salesPerson.id} value={salesPerson.id}>
                                                    {salesPerson.employee_name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleCustomersChange} value={this.state.potentialCustomer} name="potential_customer" id="potential_customer" className='form-select' required>
                                        <option value="">Choose a customer</option>
                                        {this.state.potentialCustomers.map(potentialCustomer => {
                                            return (
                                                <option key={potentialCustomer.id} value={potentialCustomer.id}>
                                                    {potentialCustomer.customer_name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleSalesPriceChange} value={this.state.salesPrice} placeholder=" Sales Price" required type="number" name="price" id="price" className="form-control" />
                                    <label htmlFor="price">Sales Price</label>
                                </div>
                                <button className="btn btn-outline-secondary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SalesRecordForm