import React from 'react'
import '../index.css'


function SalesRecordList(props) {
    return (
        <>
            <p></p>
            <h2>Sales Record</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Purchaser</th>
                        <th>Automobile</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {props.sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.sales_person.employee_name}</td>
                                <td>{sale.sales_person.employee_number}</td>
                                <td>{sale.potential_customer.customer_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>$ {new Intl.NumberFormat().format(sale.sales_price)}</td>
                                <td style={{ textAlign: "right" }}>
                                    <button className="cancel" onClick={() => props.SaleOnDelete(sale)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );

}

export default SalesRecordList;