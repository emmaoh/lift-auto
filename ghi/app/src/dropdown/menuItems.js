export const menuItems = [
    {
        title: "Home",
        href: "/"
    },
    {
        title: "Inventory",
        submenu: [
            {
                title: "Manufacturers",
                href: "/manufacturers",
            },
            {
                title: "New Manufacture",
                href: "/manufacturers/new",
            },
            {
                title: "Vehicle Models",
                href: "/models",
            },
            {
                title: "New Vehicle Model",
                href: "/models/new",
            },
            {
                title: "Automobiles",
                href: "/automobiles",
            },
            {
                title: "New Automobile",
                href: "/automobiles/new",
            },
        ]
    },
    {
        title: "Services",
        submenu: [
            {
                title: "New Technician",
                href: "/technicians/new",
            },
            {
                title: "New Appointment",
                href: "/appointments/new",
            },
            {
                title: "Appointments",
                href: "/appointments",
            },
            {
                title: "Service History",
                href: "/appointments/history",
            },
        ]
    },
    {
        title: "Sales",
        submenu: [
            {
                title: "New Salesperson",
                href: "/salespersons/new",
            },
            {
                title: "Sales Person History",
                href: "/salespersons/history",
            },
            {
                title: "New Sales Record",
                href: "/sales/new",
            },
            {
                title: "Sales Records",
                href: "/sales",
            },
            {
                title: "Potential Customer",
                href: "/potential_customers/new",
            },
        ]
    },
];