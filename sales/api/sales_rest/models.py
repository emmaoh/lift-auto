from django.db import models
from django.urls import reverse


class SalesPerson(models.Model):
    employee_name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.id})

    def __str__(self):
        return self.employee_name


class PotentialCustomer(models.Model):
    customer_name = models.CharField(max_length=100)
    customer_address = models.CharField(max_length=100)
    customer_phone_number = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_potential_customer", kwargs={"pk": self.id})

    def __str__(self):
        return self.customer_name


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    vin = models.CharField(max_length=17)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

    def __str__(self):
        return str(self.vin) + " " + str(self.id)


class Sales(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO, related_name="automobile", on_delete=models.CASCADE
    )
    sales_person = models.ForeignKey(
        SalesPerson, related_name="sales_person", on_delete=models.CASCADE
    )
    potential_customer = models.ForeignKey(
        PotentialCustomer,
        related_name="potential_customer",
        on_delete=models.CASCADE
    )
    sales_price = models.DecimalField(max_digits=10, decimal_places=2)
