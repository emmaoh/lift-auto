import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import (
    AutomobileVOEncoder,
    SalesPersonEncoder,
    PotentialCustomerEncoder,
    SalesEncoder,
)
from .models import AutomobileVO, SalesPerson, PotentialCustomer, Sales


@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        sales_persons = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_persons,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
                )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_potential_customers(request):
    if request.method == "GET":
        potential_customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customers": potential_customers},
            encoder=PotentialCustomerEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        potential_customers = PotentialCustomer.objects.create(**content)
        return JsonResponse(
            potential_customers,
            encoder=PotentialCustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_potential_customer(request, pk):
    if request.method == "GET":
        try:
            potential_customer = PotentialCustomer.objects.get(id=pk)
            return JsonResponse(
                potential_customer,
                encoder=PotentialCustomerEncoder,
                safe=False
            )
        except PotentialCustomer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            potential_customer = PotentialCustomer.objects.get(id=pk)
            PotentialCustomer.delete(self=potential_customer)
            return JsonResponse(
                potential_customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except PotentialCustomer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sales.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        print("content", content)
        automobile_id = content["automobile"]
        automobile = AutomobileVO.objects.get(id=automobile_id)
        content["automobile"] = automobile
        potential_customer_id = content["potential_customer"]
        potential_customer = PotentialCustomer.objects.get(
            id=potential_customer_id
            )
        content["potential_customer"] = potential_customer
        sales_person_id = content["sales_person"]
        sales_person = SalesPerson.objects.get(id=sales_person_id)
        content["sales_person"] = sales_person
        sales = Sales.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SalesEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sales.objects.get(id=pk)
            return JsonResponse(sale, encoder=SalesEncoder, safe=False)
        except Sales.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            sale = Sales.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except Sales.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET"])
def api_automobile_vos(request):
    automobile_vos = AutomobileVO.objects.all()
    return JsonResponse(
        automobile_vos,
        encoder=AutomobileVOEncoder,
        safe=False,
    )
